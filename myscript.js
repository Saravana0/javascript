//Getting Month
const d= new Date();
const months= ["January","February","March","April","May","June","July","August","September","October","November","December"];
document.getElementById("sample").innerHTML= months[d.getMonth()];

//Getting Day
const h= new Date();
const days= ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
document.getElementById("demo").innerHTML= days[h.getDay()];

//using Objects
const person={
    firstName: "Kaniyan",
    age: 25,
    lastName: "Poogunran",
    fullName: function(){
        return this.firstName + " " + this.lastName;
    }
};
document.getElementById("hand").innerHTML= person.fullName() + " was an influential Tamil philosopher from the Sangam age. ";